# Swedish BankID Playground

There is some stuff on experimenting with [Signicat Consent Signature](https://developer.signicat.com/documentation/signing/signing-consent-signature/) flow.

## Why Swedish and not Norwegian?

Because Norwegian BankID requires to [order & issue test SIM cards](https://developer.signicat.com/id-methods/norwegian-bankid-on-mobile/#test-information), otherwise it completely won't work even on test environment.

Rather Swedish BankID is quite more liberal, it allows to generate virtual testing users for anyone free of charge.

## What to do?

### 1. Login to Demobanken

Firstly obtain a Swedish BankID test code. It will be used for signing in into [Demo Bank](https://demo.bankid.com/) and in your Android app installation.

**Current code:** `tffV9pCq`

**Valid till:** March 17th, 2020

If the code is expired, obtain the new one [here](https://demo.bankid.com/CreateCode.aspx).

Once you got the code, [sign in](https://demo.bankid.com/).

### 2. Setup a test user for BankID

1. Click "Issue" button.
2. Fill-in the form "Issue Mobile BankID". You have to think a Swedish personal number. **Note:** the last digit must accord to LUHN. You can use [feik.se](https://fejk.se/) to generate *almost* correct personal numbers, but **don't forget to prepend** them with 2 digits of century (19 or 20).
3. If the form is filled-in properly, a modal window will pop up. Click "Open BankID issuing".
4. Now you need to install Android app. Download it from [this *pretty* Swedish-language docs](http://www.bankid.com/rp/info/), link should be named something like "Testversion BankID säkerhetsapp för Android".
5. Once you have installed Android app, launch it & use it for scanning QR code displayed after step 3.

**Current test user *personnummer*:** `200201098589`

### 3. Play with it!

You're here. Just run this Java stuff and enjoy (or suffer) =)

