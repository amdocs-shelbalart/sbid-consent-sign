package com.biel.base64;

public class Base64 {
    public static String encode(byte[] data) {
        return java.util.Base64.getEncoder().encodeToString(data);
    }

    public static String encode(String data) {
        return encode(data.getBytes());
    }

    public static String encodeUrlSafe(byte[] data) {
        String preResult = encode(data);
        String urlSafeResult = preResult
            .replace("+", "-")
            .replace("/", "_");
        while (urlSafeResult.endsWith("=")) {
            urlSafeResult = urlSafeResult.substring(0, urlSafeResult.length() - 1);
        }
        return urlSafeResult;
    }

    public static String encodeUrlSafe(String data) {
        return encodeUrlSafe(data.getBytes());
    }
}
