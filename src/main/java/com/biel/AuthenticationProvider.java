package com.biel;

import com.nimbusds.jose.JOSEException;

import java.text.ParseException;

public interface AuthenticationProvider {

    void authenticate(String personnummer, String message) throws Exception;
}
