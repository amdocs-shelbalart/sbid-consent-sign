package com.biel.UrlUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class UrlCoder {
    /** @author https://stackoverflow.com/a/37368660/2175025 */
    public static String decodeUriComponent(String uriComponent) {
        try {
            return uriComponent == null ? null : URLDecoder.decode(uriComponent, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.err.println("Unsupported encoding: must be UTF-8!!!");
            e.printStackTrace();
            return null;
        }
    }

    public static String encodeUriComponent(String uriComponent) {
        try {
            return uriComponent == null ? null : URLEncoder.encode(uriComponent, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.err.println("Unsupported encoding: must be UTF-8!!!");
            e.printStackTrace();
            return null;
        }
    }
}
