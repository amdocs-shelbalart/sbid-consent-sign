package com.biel.UrlUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.*;

public class UrlParamsParser {
    /** @author https://stackoverflow.com/a/37368660/2175025 */
    public static Map<String, List<String>> parseMap(String url) throws MalformedURLException {
        URL urlObj = new URL(url);
        return Pattern
            .compile("&")
            .splitAsStream(urlObj.getQuery())
            .map(s -> Arrays.copyOf(s.split("="), 2))
            .collect(
                groupingBy(s -> UrlCoder.decodeUriComponent(s[0]),
                mapping(s -> UrlCoder.decodeUriComponent(s[1]),
                toList()
            )));
    }

    /** @author https://stackoverflow.com/a/37368660/2175025 */
    public static List<AbstractMap.SimpleEntry<String, String>> parseList(String url) throws MalformedURLException {
        URL urlObj = new URL(url);
        return Pattern
            .compile("&")
            .splitAsStream(urlObj.getQuery())
            .map(s -> Arrays.copyOf(s.split("="), 2))
            .map(o -> new AbstractMap.SimpleEntry<>(UrlCoder.decodeUriComponent(o[0]), UrlCoder.decodeUriComponent(o[1])))
            .collect(toList());
    }
}
