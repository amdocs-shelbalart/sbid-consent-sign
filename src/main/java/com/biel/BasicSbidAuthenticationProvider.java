package com.biel;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.jwk.RSAKey;

import kong.unirest.*;
import kong.unirest.Header;
import org.json.JSONObject;
import org.json.JSONArray;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.biel.UrlUtil.UrlParamsParser;
import com.biel.UrlUtil.UrlCoder;
import com.biel.base64.Base64;
import com.biel.pkce.Generator;

/*
 Implementation based on description https://developer.signicat.com/documentation/signing/signing-consent-signature/
 */
public class BasicSbidAuthenticationProvider implements AuthenticationProvider {

    private static final boolean DEBUG = false;
    private static final String JWK = "https://preprod.signicat.com/oidc/jwks.json";
    private static final String SINDICAT_KEY_TYPE = "RSA";
    private static final String SINDICAT_KEY_USAGE = "enc";
    private static final String SINDICAT_AUTHORIZATION_URL = "https://preprod.signicat.com/oidc/authorize";
    private static final String SINDICAT_REDIRECT_URL = "https://labs.signicat.com/redirect";
    private static final String SINDICAT_STATUS_URI_KEY = "collectUrl";
    private static final String SINDICAT_STATUS_FIELD_KEY = "progressStatus";
    private static final String SINDICAT_STATUS_PENDING = "OUTSTANDING_TRANSACTION";
    private static final String SINDICAT_STATUS_FINISHED = "COMPLETE";
    private static final String SINDICAT_COMPLETE_URI_KEY = "completeUrl";
    private static final String SINDICAT_CODE_FIELD_KEY = "code";
    private static final String SINDICAT_AUTH_BASIC_TOKEN = Base64.encode("demo-inapp:mqZ-_75-f2wNsiQTONb7On4aAZ7zc218mrRVk1oufa8");
    private static final String SINDICAT_AUTH_TOKEN_URL = "https://preprod.signicat.com/oidc/token";
    private static final String SINDICAT_AUTH_TOKEN_FIELD_KEY = "access_token";
    private static final String SINDICAT_USER_INFO_URL = "https://preprod.signicat.com/oidc/userinfo";
    private static final String SINDICAT_SDO_URL = "https://preprod.signicat.com/oidc/signature";

    public void authenticate(String personnummer, String message) throws Exception {
        // Generate PKCE data
        String codeVerifier = Generator.generateCodeVerifier();
        String codeChallenge = Generator.getCodeChallenge(codeVerifier);
        System.out.println("code_verifier = " + codeVerifier);
        System.out.println("code_challenge = " + codeChallenge);

        // Amdocs corporate proxy
        Unirest.config()
            .proxy("genproxy.corp.amdocs.com", 8080)
            .enableCookieManagement(true)
            .cookieSpec("standard")
            .followRedirects(false);

        //Fetch jwks.json, prepare the public key.
        JSONObject key = findJWK();
        RSAKey jwkey = RSAKey.parse(key.toString());
        // Token payload (JSON)
        Payload payload = createPayloadAuthSubject(
            "subject-" + personnummer,
            message,
            codeChallenge
        );
        // Signicat OIDC server requires "kid" in header of JWE token
        JWEHeader jweHeader = new JWEHeader.Builder(
            JWEAlgorithm.RSA_OAEP,
            EncryptionMethod.A256CBC_HS512
        )
            .keyID(key.getString("kid"))
            .build();
        // Create a JWEObject with header and JSON payload
        JWEObject jweObject = new JWEObject(jweHeader, payload);

        // Create an encrypter with the specified public RSA key
        RSAEncrypter encrypter = new RSAEncrypter(jwkey.toPublicJWK());
        // // Encoding JWE token
        jweObject.encrypt(encrypter);

        // # STEP 1: Sent OIDC Authorization Request
        HttpResponse<JsonNode> authResponse = Unirest
            .get(SINDICAT_AUTHORIZATION_URL)
            .header("accept", "application/json")
            .queryString("request", jweObject.serialize())
            .asJson();
        JSONObject authJson = new JSONObject(authResponse
            .getBody()
            .getObject()
            .toString()
        );
        System.out.println(authJson.toString(4));
        String statusUri = (String) authJson.get(SINDICAT_STATUS_URI_KEY);
        System.out.println(statusUri);
        List<Header> authHeaders = authResponse
            .getHeaders()
            .all();
        System.out.println("STEP 1 HTTP Response Headers:");
        for (Header header : authHeaders) {
            System.out.println(header.getName() + ": " + header.getValue());
        }

        // # STEP 2: Poll statusUri until status=finished
        JSONObject statusResponse;
        String status = null;
        do {
            System.out.println("Not finished yet, sleeping for 1 sec...");
            Thread.sleep(1000); // wait for 1 sec before retrying

            System.out.println("Checking state...");

            statusResponse = new JSONObject(Unirest
                .get(statusUri)
                .header("accept", "application/json")
                .asJson()
                .getBody()
                .getObject()
                .toString()
            );
            System.out.println(statusResponse.toString(4));

            if (!statusResponse.has(SINDICAT_STATUS_FIELD_KEY)) {
                status = SINDICAT_STATUS_PENDING;
                System.out.println("Presumed state: " + status);
            } else {
                status = (String) statusResponse.get(SINDICAT_STATUS_FIELD_KEY);
                System.out.println("Got state: " + status);
            }

            if (
                !status.equals(SINDICAT_STATUS_PENDING)
                && !status.equals(SINDICAT_STATUS_FINISHED)
            ) {
                System.err.println("Unknown state: " + status);
            }
        } while (!status.equals(SINDICAT_STATUS_FINISHED));
        String completeUri = statusResponse.getString(SINDICAT_COMPLETE_URI_KEY);
        System.out.println(completeUri);

        // # STEP 3: Call completeUrl - the last redirect will contain CODE and STATE.
        ArrayList<String> redirects = new ArrayList<>();
        redirects.add(completeUri);
        HttpResponse<String> redirectResponse;
        boolean stop = false;
        int i = 1; // For debug
        do {
            final String requestUri = redirects.get(redirects.size() - 1);
            System.out.println("Requesting: " + requestUri);
            redirectResponse = Unirest
                .get(requestUri)
                .asString();
            List<Header> redirectHeaders = redirectResponse.getHeaders().all();
            String newUri = null;
            for (Header redirectHeader : redirectHeaders) {
                if (redirectHeader.getName().equals("Location")) {
                    newUri = redirectHeader.getValue();
                    System.out.println("Redirected to: " + newUri);
                    redirects.add(newUri);
                }
            }
            if (newUri == null) {
                stop = true;
            }

            // For debug
            if (DEBUG) {
                final String redirectBody = redirectResponse.getBody();
                final String fileHeadersName = "redirect-" + i + ".headers.txt";
                final String fileBodyName = "redirect-" + i + ".body.txt";
                BufferedWriter headersWriter = new BufferedWriter(new FileWriter(fileHeadersName));
                BufferedWriter bodyWriter = new BufferedWriter(new FileWriter(fileBodyName));
                for (Header redirectHeader : redirectHeaders) {
                    headersWriter.write(redirectHeader.getName() + ": " + redirectHeader.getValue() + "\n");
                }
                bodyWriter.write(redirectBody);
                headersWriter.close();
                bodyWriter.close();
                ++i;
            }
        } while (!stop);
        final String latestUri = redirects.get(redirects.size() - 1);
        System.out.println("Latest URI: " + latestUri);
        Map<String, List<String>> latestUriParams = UrlParamsParser.parseMap(latestUri);
        if (!latestUriParams.containsKey(SINDICAT_CODE_FIELD_KEY)) {
            throw new Exception("Auth Code not found!");
        }
        if (latestUriParams.get(SINDICAT_CODE_FIELD_KEY).size() != 1) {
            throw new Exception("There are multiple Auth Codes!");
        }
        final String authCode = latestUriParams.get(SINDICAT_CODE_FIELD_KEY).get(0);
        System.out.println("Parsed Auth Code: " + authCode);

        // # STEP 4: Demand access token (bearer)
        System.out.println("Auth Basic: " + SINDICAT_AUTH_BASIC_TOKEN);
        String authTokenPayload = createPayloadAuthToken(authCode, codeVerifier);
        System.out.println(authTokenPayload);
        JSONObject authTokenResponse = new JSONObject(Unirest
            .post(SINDICAT_AUTH_TOKEN_URL)
            //.header("accept", "application/json")
            .header("content-type", "application/x-www-form-urlencoded")
            .header("authorization", "Basic " + SINDICAT_AUTH_BASIC_TOKEN)
            .body(authTokenPayload)
            .asJson()
            .getBody()
            .getObject()
            .toString()
        );
        System.out.println(authTokenResponse.toString(4));
        final String authBearerToken = "Bearer " + authTokenResponse.getString(SINDICAT_AUTH_TOKEN_FIELD_KEY);
        System.out.println(authBearerToken);

        // # STEP 5: Get user info
        JSONObject userInfoResponse = new JSONObject(Unirest
            .get(SINDICAT_USER_INFO_URL)
            .header("accept", "application/json")
            .header("authorization", authBearerToken)
            .asJson()
            .getBody()
            .getObject()
            .toString()
        );
        System.out.println("User Info: " + userInfoResponse.toString(4));

        // # STEP 6: Download SDO (signed data object)
        String sdoBody = Unirest
            .get(SINDICAT_SDO_URL)
            .header("accept", "application/xml")
            .header("authorization", authBearerToken)
            .asString()
            .getBody();
        // Write SDO to file
        final String fileSdoName = "sdo.xml";
        BufferedWriter sdoWriter = new BufferedWriter(new FileWriter(fileSdoName));
        sdoWriter.write(sdoBody);
        sdoWriter.close();
    }

    private Payload createPayloadAuthSubject(String personnummer, String message, String codeChallenge) {
        net.minidev.json.JSONObject payload_json = new net.minidev.json.JSONObject();
        List<String> login_hint = new ArrayList<String>();
        login_hint.add(personnummer);
        payload_json.put("login_hint", login_hint);
        payload_json.put("ui_locales", "en");
        payload_json.put("scope", "openid profile signicat.sign");
        payload_json.put("signicat_signtext", message);
        payload_json.put("acr_values", "urn:signicat:oidc:method:sbid-inapp-sign");
        payload_json.put("response_type", "code");
        payload_json.put("redirect_uri", SINDICAT_REDIRECT_URL);
        payload_json.put("state", "ABCDEF012345");
        payload_json.put("client_id", "demo-inapp-sign");
        //payload_json.put("code_challenge", codeChallenge);
        //payload_json.put("code_challenge_method", "S256");
        Payload payload = new Payload(payload_json);
        return payload;
    }

    private String createPayloadAuthToken(String authCode, String codeVerifier) {
        ArrayList<String> paramsKV = new ArrayList<>();
        //paramsKV.add("client_id" + "=" + "demo-inapp-sign");
        paramsKV.add("grant_type" + "=" + "authorization_code");
        paramsKV.add("code" + "=" + authCode);
        //paramsKV.add("code_verifier" + "=" + codeVerifier);
        paramsKV.add("redirect_uri" + "=" + UrlCoder.encodeUriComponent(SINDICAT_REDIRECT_URL));
        //paramsKV.add("redirect_uri" + "=" + SINDICAT_REDIRECT_URL);

        return String.join("&", paramsKV);
    }

    private JSONObject findJWK() throws Exception {
        JSONArray keys = new JSONArray(Unirest.get(JWK)
            .asJson()
            .getBody()
            .getObject()
            .getJSONArray("keys")
            .toString()
        );

        for (Object key : keys) {
            final JSONObject keyCasted = (JSONObject) key;
            final String keyType  = keyCasted.getString("kty");
            final String keyUsage = keyCasted.getString("use");
            if (
                keyType.equals(SINDICAT_KEY_TYPE)
                && keyUsage.equals(SINDICAT_KEY_USAGE)
            ) {
                return keyCasted;
            }
        }

        throw new Exception("No key found!");
    }

    public static void main(String[] args) throws ParseException, JOSEException, Exception {
        BasicSbidAuthenticationProvider basicSbidAuthenticationProvider = new BasicSbidAuthenticationProvider();

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String message = "Currently: " + dateFormat.format(date);

        basicSbidAuthenticationProvider.authenticate("200201098589", message);
    }
}
