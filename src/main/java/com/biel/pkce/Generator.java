package com.biel.pkce;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import com.biel.base64.Base64;

public class Generator {
    public static String generateCodeVerifier(int numBytes) throws Exception {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[numBytes];
        random.nextBytes(bytes);
        String result = Base64.encodeUrlSafe(bytes);

        if (result.length() < 43) {
            throw new Exception("Generated code_verifier is too short: " + result.length() + ", must be at least 43 according to RFC 7636 section 4.1");
        }
        if (result.length() > 128) {
            result = result.substring(0, 127);
        }

        return result;
    }

    public static String generateCodeVerifier() throws Exception {
        return generateCodeVerifier(64);
    }

    public static String getCodeChallenge(String verifier) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] sha256 = digest.digest(verifier.getBytes());
        String result = Base64.encodeUrlSafe(sha256);

        if (result.length() < 43) {
            throw new Exception("Generated code_challenge is too short: " + result.length() + ", must be at least 43 according to RFC 7636 section 4.2");
        }
        if (result.length() > 128) {
            throw new Exception("Generated code_challenge is too long: " + result.length() + ", must be at max 128 according to RFC 7636 section 4.2");
        }

        return result;
    }
}
